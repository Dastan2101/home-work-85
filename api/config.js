const path = require('path');

const rootPath = __dirname;

module.exports = {
    rootPath,
    uploadPath: path.join(rootPath, 'public/uploads'),
    dbUrl: 'mongodb://localhost/radioFM',
    mongoOptions: {useNewUrlParser: true, useCreateIndex: true},
    facebook: {
        appId: '820417251677917',
        appSecret: 'a2e80e19268a2e5cb31930cb10f5a08b' // insecure
    }
};
