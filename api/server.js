const express = require('express');
const mongoose = require('mongoose');
const config = require('./config');
const cors = require('cors');

const artist = require('./app/artist');
const album = require('./app/album');
const track = require('./app/track');
const users = require('./app/users');
const history = require('./app/history');
const admin = require('./app/admin');

const app = express();

app.use(express.json());
app.use(express.static('public'));
app.use(cors());

const port = 8000;

mongoose.connect(config.dbUrl, config.mongoOptions).then(() => {
    app.use('/artists', artist);
    app.use('/albums', album);
    app.use('/tracks', track);
    app.use('/users', users);
    app.use('/track_history', history);
    app.use('/admin', admin);

    app.listen(port, () => {
        console.log(`Server started on ${port} port`);
    });

});

