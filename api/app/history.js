const express = require('express');
const TrackHistory = require('../models/TrackHistory');
const auth = require('../middleware/auth');

const router = express.Router();

router.post('/', auth, async (req, res) => {

    let dataHistory = req.body;

    dataHistory.user = req.user._id;
    dataHistory.datetime = new Date().toISOString();

    const historyTracks = new TrackHistory(dataHistory);

    historyTracks.save()
        .then(result => res.send(result))
        .catch(error => res.sendStatus(error))
});

router.get('/', auth, (req, res) => {
    TrackHistory.find({user: req.user._id}).populate({
        path: 'track',
        populate: {model: 'Album', path: 'album', populate: {model: 'Artist', path: 'artist'}}
    }).sort({datetime: -1})
        .then(result => res.send(result))
        .catch(error => res.sendStatus(error))
});

module.exports = router;