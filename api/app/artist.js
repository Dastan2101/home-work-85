const express = require('express');
const Artist = require('../models/Artist');
const multer = require('multer');
const path = require('path');
const config = require('../config');
const nanoid = require('nanoid');

const auth = require('../middleware/auth');
const tryAuth = require('../middleware/tryAuth');

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname));
    }
});

const upload = multer({storage});

const router = express.Router();

router.get('/', tryAuth, async (req, res) => {

    let criteria = {published: true};

    if (req.user) {
        criteria = {
            $or: [
                {published: true},
                {user: req.user._id}]
        }
    }

    const artists = await Artist.find(criteria);

    res.send(artists)

});

router.post('/', [auth, upload.single('image')], async (req, res) => {

    try {
        const artist = await new Artist({
            title: req.body.title,
            user: req.user._id,
            image: req.file ? req.file.filename : null
        });

        await artist.save();

        res.send(artist)

    } catch (error) {
        res.sendStatus(error);
    }

});

module.exports = router;