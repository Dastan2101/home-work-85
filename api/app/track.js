const express = require('express');
const Track = require('../models/Track');
const Album = require('../models/Album');

const router = express.Router();

router.get('/', (req, res) => {

    if (req.query.album) {
        Track.find({album: req.query.album}).sort({number: 1}).populate("album")
            .then(track => res.send(track))
            .catch(() => res.sendStatus(500))
    } else if (req.query.artist_id) {
        Album.find({artist: req.query.artist_id}).sort({number: 1})
            .then(albums => {
                Promise.all(albums.map(album => Track.find({album: album._id})))
                    .then(result => res.send(result))
                    .catch(e => res.status(500).send(e))
            })
    } else {
        Track.find()
            .then(track => res.send(track))
            .catch(() => res.sendStatus(500))
    }

});

router.post('/', async (req, res) => {

    try {
        const trackBody = req.body;

        const tracks = await Track.find({album: trackBody.album});

        trackBody.number = tracks.length + 1;

        const track = await new Track(trackBody);

        await track.save();
        res.send(tracks)

    } catch (error) {
        res.sendStatus(error)
    }
});


module.exports = router;