const express = require('express');
const Album = require('../models/Album');
const multer = require('multer');
const path = require('path');
const config = require('../config');
const nanoid = require('nanoid');

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname));
    }
});

const upload = multer({storage});
const router = express.Router();

router.get('/', (req, res) => {
    if (req.query.artist) {
        Album.find({artist: req.query.artist}).sort({yearOfIssue: 1}).populate("artist")
            .then(album => res.send(album))
            .catch(() => res.sendStatus(500))
    } else {
        Album.find()
            .then(album => res.send(album))
            .catch(() => res.sendStatus(500))
    }


});

router.get('/:id', (req, res) => {

    Album.findById(req.params.id).populate("artist")
        .then(album => res.send(album))
        .catch(() => res.sendStatus(500))


});

router.post('/', upload.single('image'), (req, res) => {
    const albumData = req.body;

    if (req.file) {
        albumData.image = req.file.filename;
    }

    const album = new Album(albumData);

    album.save()
        .then(result => res.send(result))
        .catch(error => res.sendStatus(error));
});

module.exports = router;