const nanoid = require('nanoid');

const mongoose = require('mongoose');
const config = require('./config');

const Artist = require('./models/Artist');
const Album = require('./models/Album');
const Track = require('./models/Track');
const User = require('./models/Users');

const run = async () => {
    await mongoose.connect(config.dbUrl, config.mongoOptions);
    const connection = mongoose.connection;

    const collections = await connection.db.collections();

    for (let collection of collections) {
        await collection.drop();
    }

    const user = await User.create({
            username: "user-1",
            password: "123",
            role: "user",
            token: nanoid(),
            displayName: 'User One'
        },
        {
            username: "user-2",
            password: "123",
            role: "user",
            token: nanoid(),
            displayName: 'User Two'
        },
        {
            username: 'admin',
            password: '123',
            role: 'admin',
            token: nanoid(),
            displayName: 'Admin'
        });

    const artist = await Artist.create(
        {
            title: 'Eminem',
            information: 'Marshall Bruce Mathers 3',
            image: 'Eminem.jpg',
            published: true,
            user: user[0]._id
        },
        {
            title: 'Akon',
            information: 'Aliaune Damala Bouga Time Bongo Puru Nacka Lu Lu Lu Badara Akon Thiam',
            image: 'Acon.jpg',
            published: true,
            user: user[0]._id
        },
        {
            title: 'Rihanna',
            information: 'Robyn Rihanna Fenty',
            image: 'Rihanna.jpeg',
            published: true,
            user: user[0]._id
        },
        {
            title: 'Jah Khalib',
            information: 'Бахтияр Мамедов',
            image: 'Jahkhalib.jpg',
            published: true,
            user: user[1]._id
        },
        {
            title: 'Imagine Dragons',
            information: 'Imagine Dragons is an American pop rock band from Las Vegas, Nevada, consisting of lead vocalist Dan Reynolds, lead guitarist Wayne Sermon, bassist Ben McKee, and drummer Daniel Platzman.',
            image: 'imaginedragons.jpg',
            published: true,
            user: user[1]._id
        }
    );

    const album = await Album.create(
        {
            title: 'Kamikaze',
            artist: artist[0]._id,
            yearOfIssue: 2017,
            image: 'Kamikaze.jpg',
            published: true
        },
        {
            title: 'Infinite',
            artist: artist[0]._id,
            yearOfIssue: 2015,
            image: 'infinite.jpg',
            published: true
        },
        {
            title: 'Revival',
            artist: artist[0]._id,
            yearOfIssue: 2016,
            image: 'Revival.jpg',
            published: true
        },
        {
            title: 'Freedom',
            artist: artist[1]._id,
            yearOfIssue: 2008,
            image: 'Freedom.jpg',
            published: true
        },
        {
            title: 'Trouble',
            artist: artist[1]._id,
            yearOfIssue: 2004,
            image: 'Trouble.jpg',
            published: true
        },
        {
            title: 'In Your Eyes',
            artist: artist[1]._id,
            yearOfIssue: 2005,
            image: 'InYourEyes.jpg',
            published: true
        },
        {
            title: 'Music of the Sun',
            artist: artist[2]._id,
            yearOfIssue: 2005,
            image: 'Music_of_the_Sun.jpg',
            published: true
        },
        {
            title: 'Music of Night',
            artist: artist[2]._id,
            yearOfIssue: 2007,
            image: 'musicOfTheNight.jpg',
            published: true
        },
        {
            title: 'Всё, что мы любим',
            artist: artist[3]._id,
            yearOfIssue: 2016,
            image: 'vsechtomy.jpg',
            published: true
        },
        {
            title: 'Джазовый грув',
            artist: artist[3]._id,
            yearOfIssue: 2017,
            image: 'jazzOviy.jpg',
            published: true
        },
        {
            title: 'Origins',
            artist: artist[4]._id,
            yearOfIssue: 2014,
            image: 'Origins.jpg',
            published: true
        },
        {
            title: 'Night Visions',
            artist: artist[4]._id,
            yearOfIssue: 2018,
            image: 'NightVisions.jpg',
            published: true
        }
    );


    await Track.create(
        {
            title: 'The Ringer',
            album: album[0]._id,
            duration: '5:37',
            number: 1,
            url: 'https://www.youtube.com/embed/ryr75N0nki0?autoplay=1',
            published: true
        },
        {
            title: 'Walk on Water',
            album: album[1]._id,
            duration: '5:04',
            number: 1,
            url: 'https://www.youtube.com/embed/ryr75N0nki0?autoplay=1',
            published: true
        },
        {
            title: 'Greatest',
            album: album[2]._id,
            duration: '3:46',
            number: 1,
            url: 'https://www.youtube.com/embed/ryr75N0nki0?autoplay=1',
            published: true
        },
        {
            title: 'Breeze',
            album: album[2]._id,
            duration: '3:16',
            number: 2,
            url: 'https://www.youtube.com/embed/ryr75N0nki0?autoplay=1',
            published: true
        },
        {
            title: 'Right Now (Na Na Na)',
            album: album[3]._id,
            duration: '3:95',
            number: 1,
            url: 'https://www.youtube.com/embed/ryr75N0nki0?autoplay=1',
            published: true
        },

        {
            title: 'Beautiful',
            album: album[4]._id,
            duration: '4:01',
            number: 1,
            url: 'https://www.youtube.com/embed/ryr75N0nki0?autoplay=1',
            published: true
        },
        {
            title: 'Moon',
            album: album[4]._id,
            duration: '4:31',
            number: 2,
            url: 'https://www.youtube.com/embed/ryr75N0nki0?autoplay=1',
            published: true
        },
        {
            title: 'Put it up',
            album: album[5]._id,
            duration: '5:12',
            number: 2,
            url: 'https://www.youtube.com/embed/ryr75N0nki0?autoplay=1',
            published: true
        },
        {
            title: 'Here I Go Again',
            album: album[6]._id,
            duration: '4:11',
            number: 1,
            url: 'https://www.youtube.com/embed/ryr75N0nki0?autoplay=1',
            published: true
        },
        {
            title: 'I am here without',
            album: album[6]._id,
            duration: '4:43',
            number: 2,
            url: 'https://www.youtube.com/embed/ryr75N0nki0?autoplay=1',
            published: true
        },
        {
            title: 'When we go',
            album: album[7]._id,
            duration: '2:11',
            number: 1,
            url: 'https://www.youtube.com/embed/ryr75N0nki0?autoplay=1',
            published: true
        },
        {
            title: 'Here I Go Again',
            album: album[7]._id,
            duration: '2:11',
            number: 2,
            url: 'https://www.youtube.com/embed/ryr75N0nki0?autoplay=1',
            published: true
        },
        {
            title: 'What are you?',
            album: album[8]._id,
            duration: '4:11',
            number: 1,
            url: 'https://www.youtube.com/embed/ryr75N0nki0?autoplay=1',
            published: true
        },
        {
            title: 'Who are you?',
            album: album[8]._id,
            duration: '4:43',
            number: 2,
            url: 'https://www.youtube.com/embed/ryr75N0nki0?autoplay=1',
            published: true
        },
        {
            title: 'Again',
            album: album[9]._id,
            duration: '2:45',
            number: 1,
            url: 'https://www.youtube.com/embed/ryr75N0nki0?autoplay=1',
            published: true
        },
        {
            title: 'So sweet',
            album: album[9]._id,
            duration: '4:32',
            number: 2,
            url: 'https://www.youtube.com/embed/ryr75N0nki0?autoplay=1',
            published: true
        },
        {
            title: 'So cool',
            album: album[10]._id,
            duration: '5:32',
            number: 1,
            url: 'https://www.youtube.com/embed/ryr75N0nki0?autoplay=1',
            published: true
        },
        {
            title: 'Come with me',
            album: album[11]._id,
            duration: '3:12',
            number: 1,
            url: 'https://www.youtube.com/embed/ryr75N0nki0?autoplay=1',
            published: true
        }
    );

    await connection.close();

};

run().catch(error => {
    console.error('Something went', error);
});