import React, {Component, Fragment} from 'react';
import {withRouter} from "react-router-dom";
import Navigation from "./components/Toolbar/Navigation";
import {connect} from "react-redux";
import {logoutUser} from "./store/actions/Actions";
import Routes from "./Routes";

class App extends Component {
    render() {
        return (
            <Fragment>
                <header>
                    <Navigation
                        user={this.props.user}
                        logoutUser={this.props.logoutUser}
                        quantity={this.props.quantity}
                    />
                </header>
                <div style={{width: '1200px', margin: '50px auto'}}>
                    <Routes
                        user={this.props.user}
                    />
                </div>
            </Fragment>
        );
    }
}

const mapStateToProps = state => ({
    user: state.users.user,
    quantity: state.admin.quantity
});

const mapDispatchToProps = dispatch => ({
    logoutUser: () => dispatch(logoutUser())
});

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(App));
