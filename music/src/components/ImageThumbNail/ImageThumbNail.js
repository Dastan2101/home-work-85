import React from 'react';
import {apiURL} from "../../constants";

import notImage from '../../assets/image/background.jpg';
import CardMedia from "@material-ui/core/CardMedia/CardMedia";

const styles = {
    backgroundSize: 'curtain',
    width: '100%'
};

const ImageThumbnail = (props) => {
    let image = notImage;

    if (props.image) {
        image = apiURL + '/uploads/' + props.image;
    }

    return <CardMedia
        component="img"
        style={styles}
        height="230"
        image={image}
    />;
};

export default ImageThumbnail;
