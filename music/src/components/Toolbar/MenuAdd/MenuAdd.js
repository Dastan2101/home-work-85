import React, {Component} from 'react';
import Button from '@material-ui/core/Button';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import {withStyles} from '@material-ui/core/styles';
import {Link as RouterLink} from 'react-router-dom'
import Link from '@material-ui/core/Link';

const styles = theme => ({
    button: {
        margin: theme.spacing.unit,
    },
    margin: {
        margin: theme.spacing.unit,
    },
});

class MenuAdd extends Component {
    state = {
        anchorEl: null,
    };

    handleClick = event => {
        this.setState({anchorEl: event.currentTarget});
    };

    handleClose = () => {
        this.setState({anchorEl: null});
    };

    render() {
        const {anchorEl} = this.state;

        return (
            <div>
                <Button
                    aria-owns={anchorEl ? 'simple-menu' : undefined}
                    aria-haspopup="true"
                    onClick={this.handleClick}
                    style={{color: '#ffff'}}
                >
                    Add
                </Button>
                <Menu
                    id="simple-menu"
                    anchorEl={anchorEl}
                    open={Boolean(anchorEl)}
                    onClose={this.handleClose}
                >
                    <Link component={RouterLink} to="/add_artist">
                        <MenuItem onClick={this.handleClose}>Artist</MenuItem>
                    </Link>
                    <Link component={RouterLink} to="/add_album">
                        <MenuItem onClick={this.handleClose}>Album</MenuItem>
                    </Link>
                    <Link  component={RouterLink} to="/add_track">
                        <MenuItem onClick={this.handleClose}>Track</MenuItem>
                    </Link>

                </Menu>
            </div>
        );
    }
}

export default withStyles(styles)(MenuAdd);