import React, {Fragment} from 'react';
import {withStyles} from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import {Link} from 'react-router-dom'
import IconButton from '@material-ui/core/IconButton';
import Badge from '@material-ui/core/Badge';
import NotificationsIcon from '@material-ui/icons/Notifications';
import Avatar from '@material-ui/core/Avatar';

import MenuAdd from "./MenuAdd/MenuAdd";
import {apiURL} from "../../constants";

const styles = {
    root: {
        flexGrow: 1,
    },
    grow: {
        flexGrow: 1,
    },
    menuButton: {
        marginLeft: -12,
        marginRight: 20,
    },
    title: {
        marginRight: 30,
        marginLeft: 20
    },
    bigAvatar: {
        width: 60,
        height: 60,
        margin: 10,

    },
};
const Navigation = ({user, logoutUser, quantity}) => {
    return (
        <div style={styles.root}>
            <AppBar position="static">
                <Toolbar>
                    <Typography variant="h6" color="inherit" style={styles.grow}>
                        <Button color="inherit" component={Link} to="/">
                            Music
                        </Button>
                    </Typography>
                    {!user ? (
                        <Fragment>
                            <Button color="inherit" component={Link} to="/registration">Sign Up</Button>
                            <Button color="inherit" component={Link} to="/login">Login</Button>
                        </Fragment>
                    ) : <Fragment>
                        <Avatar alt={user.name}
                                src={user.image}
                                style={styles.bigAvatar}>
                        </Avatar>

                        <h4 style={styles.title}>
                            {"Hello, " + user.displayName.toUpperCase()}
                        </h4>
                        {user && user.role === 'admin' ? <IconButton color="inherit" component={Link} to="/admin">
                            <Badge badgeContent={quantity} color="secondary">
                                <NotificationsIcon/>
                            </Badge>
                        </IconButton> : null}

                        <MenuAdd/>
                        <Button color="inherit" component={Link} to="/track_history">Track History</Button>
                        <Button color="inherit" onClick={logoutUser}>Logout</Button>
                    </Fragment>
                    }
                </Toolbar>
            </AppBar>
        </div>
    );
};

export default withStyles(styles)(Navigation);