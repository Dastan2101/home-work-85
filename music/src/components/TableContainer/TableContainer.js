import React from 'react';
import Table from "@material-ui/core/Table/Table";
import TableBody from "@material-ui/core/TableBody/TableBody";
import TableRow from "@material-ui/core/TableRow/TableRow";
import TableCell from "@material-ui/core/TableCell/TableCell";
import IconButton from "@material-ui/core/IconButton/IconButton";
import Button from "@material-ui/core/Button/Button";
import Paper from "@material-ui/core/Paper/Paper";

import image from '../../assets/image/background.jpg'

const styles = theme => ({
    tableRoot: {
        width: '100%',
        marginTop: theme.spacing.unit * 3,
        overflowX: 'auto',
    },
    table: {
        minWidth: 700,
    },
    margin: {
        margin: theme.spacing.unit,
    },
});
const TableContainer = ({array, onClick, onDelete}) => {

    return (
        <Paper className={styles.tableRoot}>
            <Table className={styles.table}>
                <TableBody>
                    {array.map(item => (
                        <TableRow key={item._id}>
                            <TableCell>{item.title}</TableCell>
                            <TableCell align="left">{item.information}</TableCell>
                            <TableCell align="left"><img
                                src={item.image ? 'http://localhost:8000/uploads/' + item.image : image}
                                alt="#"
                                style={{width: '30px', height: '30px'}}/>
                            </TableCell>
                            <TableCell>{item.published ?
                                <IconButton aria-label="Delete" className={styles.margin}
                                            onClick={() => onDelete(item._id)}>
                                    <i className="material-icons">
                                        delete
                                    </i>
                                </IconButton> :
                                <Button aria-label="Save" className={styles.margin}
                                        onClick={() => onClick(item._id)}>
                                    <i className="material-icons">
                                        save
                                    </i> </Button>}
                            </TableCell>

                        </TableRow>
                    ))}
                </TableBody>
            </Table>
        </Paper>
    );
};

export default TableContainer;