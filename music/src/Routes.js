import React from 'react';
import MainPage from "./container/MainPage/MainPage";
import AlbumsOfArtist from "./container/AlbumsOfArtust/AlbumsOfArtist";
import TracksList from "./container/TracksList/TracksList";
import Registr from "./container/Registr/Registr";
import Login from "./container/Login/Login";
import TrackHistory from "./container/TrackHistory/TrackHistory";
import AddArtistForm from "./container/AddForm/AddArtistForm/AddArtistForm";
import AddAlbumForm from "./container/AddForm/AddAlbumForm/AddAlbumForm";
import AddTrackForm from "./container/AddForm/AddTrackForm/AddTrackForm";

import {Redirect, Route, Switch} from "react-router-dom";
import Admin from "./container/Admin/Admin";

const ProtectedRoute = ({isAllowed, ...props}) => {
    return isAllowed ? <Route {...props} /> : <Redirect to="/login"/>
};

const Routes = ({user}) => {
    return (
        <Switch>
            <Route path="/" exact component={MainPage}/>
            <Route path="/albums/:id" exact component={AlbumsOfArtist}/>
            <Route path="/tracks/:id" exact component={TracksList}/>
            <ProtectedRoute
                isAllowed={user && user.role === 'admin'}
                path="/admin/"
                exact
                component={Admin}
            />
            <Route path="/registration" exact component={Registr}/>
            <Route path="/login" exact component={Login}/>
            <Route path="/track_history" exact component={TrackHistory}/>
            <Route path="/add_artist" exact component={AddArtistForm}/>
            <Route path="/add_album" exact component={AddAlbumForm}/>
            <Route path="/add_track" exact component={AddTrackForm}/>
        </Switch>
    );
};

export default Routes;