import axios from '../../axios-api';
import {push} from "connected-react-router";

export const FETCH_ARTIST_SUCCESS = 'FETCH_ARTIST_SUCCESS';
export const FETCH_ALBUMS_OF_ARTIST_SUCCESS = 'FETCH_ALBUMS_OF_ARTIST_SUCCESS';
export const FETCH_ALBUMS_TRACK_SUCCESS = 'FETCH_ALBUMS_TRACK_SUCCESS';

export const REGISTER_USER_SUCCESS = 'REGISTER_USER_SUCCESS';
export const REGISTER_USER_FAILURE = 'REGISTER_USER_FAILURE';

export const LOGIN_USER_SUCCESS = 'LOGIN_USER_SUCCESS';
export const LOGIN_USER_FAILURE = 'LOGIN_USER_FAILURE';
export const LOGOUT_USER = 'LOGOUT_USER';

export const FETCH_TOKEN_SUCCESS = 'FETCH_TOKEN_SUCCESS';

export const FETCH_TRACK_HISTORY_SUCCESS = 'FETCH_TRACK_HISTORY_SUCCESS';

export const FETCH_ALL_ENTITIES_SUCCESS = 'FETCH_ALL_ENTITIES_SUCCESS';

const registerUserSuccess = () => ({type: REGISTER_USER_SUCCESS});
const registerUserFailure = error => ({type: REGISTER_USER_FAILURE, error});

const loginUserSuccess = user => ({type: LOGIN_USER_SUCCESS, user});
const loginUserFailure = error => ({type: LOGIN_USER_FAILURE, error});

const fetchToken = token => ({type: FETCH_TOKEN_SUCCESS, token});

const fetchArtists = data => ({type: FETCH_ARTIST_SUCCESS, data});
const fetchAlbums = data => ({type: FETCH_ALBUMS_OF_ARTIST_SUCCESS, data});
const fetchTracks = data => ({type: FETCH_ALBUMS_TRACK_SUCCESS, data});

const fetchTrackHistorySuccess = tracks => ({type: FETCH_TRACK_HISTORY_SUCCESS, tracks});

const fetchAllEntities = data => ({type: FETCH_ALL_ENTITIES_SUCCESS, data});

export const getArtists = () => {
    return dispatch => {
        return axios.get('/artists').then(
            response => dispatch(fetchArtists(response.data))
        )
    }
};

export const createArtist = data => {
    return dispatch => {
        return axios.post('/artists', data).then(
            dispatch(push('/'))
        )
    }
};

export const getAlbums = (id) => {
    return dispatch => {
        return axios.get('/albums?artist=' + id).then(
            response => dispatch(fetchAlbums(response.data)),
        )
    }
};

export const createAlbum = data => {
    return dispatch => {
        return axios.post('albums', data).then(
            dispatch(push('/'))
        )
    }
};

export const getTracks = id => {
    return dispatch => {
        return axios.get('tracks?album=' + id).then(
            response => dispatch(fetchTracks(response.data))
        )
    }
};

export const createTrack = data => {
    return dispatch => {
        return axios.post('/tracks', data).then(
            dispatch(push('/'))
        )
    }
};

export const registerUser = userData => {
    return dispatch => {
        return axios.post('/users', userData).then(
            response => {
                dispatch(registerUserSuccess());
                dispatch(push('/login'));
            },
            error => {
                if (error.response && error.response.data) {
                    dispatch(registerUserFailure(error.response.data));
                } else {
                    dispatch(registerUserFailure({global: 'No connection'}));
                }
            }
        );
    };
};

export const loginUser = userData => {
    return dispatch => {
        return axios.post('/users/sessions', userData).then(
            response => {
                dispatch(loginUserSuccess(response.data.user));
                dispatch(fetchToken(response.data));
                dispatch(push('/'));
            },
            error => {
                if (error.response && error.response.data) {
                    dispatch(loginUserFailure(error.response.data));
                } else {
                    dispatch(loginUserFailure({global: 'No connection'}));
                }
            }
        );
    };
};

export const logoutUser = () => {
    return dispatch => {
        return axios.delete('/users/sessions').then(
            () => {
                dispatch({type: LOGOUT_USER});
            }
        )
    }
};

export const postHistoryTrack = trackId => {
    return dispatch => {
        const track = {
            track: trackId
        };
        return axios.post('/track_history', track);
    };
};

export const getTrackHistory = () => {
    return dispatch => {
        return axios.get('/track_history').then(
            response => dispatch(fetchTrackHistorySuccess(response.data))
        );
    }
};

export const getAllEntities = () => {
    return dispatch => {
        return axios.get('/admin').then(
            response => dispatch(fetchAllEntities(response.data))
        )
    }
};

export const createPublish = (id) => {
    return dispatch => {
        return axios.post('/admin/' + id + '/publish').then(
            response => dispatch(fetchAllEntities(response.data))
        )
    }
};

export const deletePublic = (id) => {
    return dispatch => {
        return axios.delete('/admin/' + id).then(
            response => dispatch(fetchAllEntities(response.data))
        );
    };
};

export const facebookLogin = (userData) => {
    console.log(userData);
    return dispatch => {
        axios.post('/users/facebookLogin', userData).then(
            response => {
                dispatch(loginUserSuccess(response.data.user));
                dispatch(push('/'))
            },
            () => {
                dispatch(loginUserFailure('Login failed!'))
            }
        )
    }
}