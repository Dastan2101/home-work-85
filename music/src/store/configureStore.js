import {createBrowserHistory} from "history";
import {applyMiddleware, combineReducers, compose, createStore} from "redux";
import {connectRouter, routerMiddleware} from "connected-react-router";
import usersReducer from "./reducers/usersReducer";
import thunkMiddleware from "redux-thunk";
import {loadState, saveState} from "./localStorage";
import axios from '../axios-api';
import trackHistory from "./reducers/trackHistoryReducer";
import artistReducer from "./reducers/artistReducer";
import albumReducer from "./reducers/albumReducer";
import trackReducer from "./reducers/trackReducer";
import adminReducer from "./reducers/adminReducer";

export const history = createBrowserHistory();

const rootReducer = combineReducers({
    router: connectRouter(history),
    trackHistoryReducer: trackHistory,
    artists: artistReducer,
    album: albumReducer,
    track: trackReducer,
    users: usersReducer,
    admin: adminReducer
});

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const middleware = [
    thunkMiddleware,
    routerMiddleware(history)
];


const enhancers = composeEnhancers(applyMiddleware(...middleware));

const persistedState = loadState();

const store = createStore(rootReducer, persistedState, enhancers);

store.subscribe(() => {
    saveState({
        users: {
            user: store.getState().users.user
        }
    })
});

axios.interceptors.request.use(config => {
    try {
        config.headers["Authorization"] = store.getState().users.user.token;
    } catch (e) {
        // user is not logged
    }

    return config;
});

export default store;