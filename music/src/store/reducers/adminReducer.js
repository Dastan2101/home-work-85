import {
    FETCH_ALL_ENTITIES_SUCCESS,
} from "../actions/Actions";


const initialState = {
    artists: [],
    albums: [],
    tracks: [],
    quantity: null
};


const adminReducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_ALL_ENTITIES_SUCCESS:
            return {
                ...state,
                artists: action.data.artists,
                albums: action.data.albums,
                tracks: action.data.tracks,
                quantity: action.data.artists.filter(res => res.published === false).length + action.data.albums.filter(res => res.published === false).length + action.data.tracks.filter(res => res.published === false).length
            };
        default:
            return state;
    }
};

export default adminReducer;