import {
    FETCH_TRACK_HISTORY_SUCCESS
} from "../actions/Actions";


const initialState = {
    artists: [],
    albums: null,
    tracks: null,
    tracksHistory: []
};
const trackHistoryReducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_TRACK_HISTORY_SUCCESS:
            return {...state, tracksHistory: action.tracks};
        default:
            return state;
    }
};

export default trackHistoryReducer;