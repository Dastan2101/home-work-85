import {
    FETCH_ARTIST_SUCCESS,
} from "../actions/Actions";


const initialState = {
    artists: [],
};
const artistReducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_ARTIST_SUCCESS:
            return {...state, artists: action.data};
        default:
            return state;
    }
};

export default artistReducer;