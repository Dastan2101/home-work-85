import {
    FETCH_ALBUMS_OF_ARTIST_SUCCESS,
} from "../actions/Actions";


const initialState = {
    albums: [],
};
const albumReducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_ALBUMS_OF_ARTIST_SUCCESS:
            return {...state, albums: action.data};
        default:
            return state;
    }
};

export default albumReducer;