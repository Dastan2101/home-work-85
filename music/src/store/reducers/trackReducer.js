import {
    FETCH_ALBUMS_TRACK_SUCCESS,
} from "../actions/Actions";


const initialState = {
    artists: [],
    albums: null,
    tracks: null,
    tracksHistory: []
};
const trackReducer= (state = initialState, action) => {
    switch (action.type) {
        case FETCH_ALBUMS_TRACK_SUCCESS:
            return {...state, tracks: action.data};
        default:
            return state;
    }
};

export default trackReducer;