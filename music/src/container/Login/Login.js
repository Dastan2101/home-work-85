import React, {Component} from 'react';
import TextField from "@material-ui/core/TextField/TextField";
import {withStyles} from '@material-ui/core/styles';
import Button from "@material-ui/core/Button/Button";
import Icon from '@material-ui/core/Icon';
import FormGroup from "@material-ui/core/FormGroup/FormGroup";
import {loginUser} from "../../store/actions/Actions";
import Modal from '@material-ui/core/Modal';
import {connect} from "react-redux";
import Typography from "@material-ui/core/Typography/Typography";
import FacebookLogin from "../../components/FacebookLogin/FacebookLogin";

const styles = theme => ({
    container: {
        display: 'flex',
        flexWrap: 'wrap',
        width: 250,
        margin: '0 auto'
    },
    textField: {
        marginLeft: theme.spacing.unit,
        marginRight: theme.spacing.unit,
        width: 250,
        alignSelf: 'center'
    },
    button: {
        margin: theme.spacing.unit,
    },
    leftIcon: {
        marginLeft: theme.spacing.unit,
    },
    paper: {
        position: 'absolute',
        top: 100,
        left: '50%',
        marginLeft: -250,
        width: theme.spacing.unit * 50,
        backgroundColor: theme.palette.background.paper,
        boxShadow: theme.shadows[5],
        padding: theme.spacing.unit * 4,
        outline: 'none',
    }
});

class Login extends Component {

    state = {
        username: '',
        password: '',
        open: true
    };
    inputChangeHandler = event => {
        this.setState({
            [event.target.name]: event.target.value
        });
    };

    submitFormHandler = event => {
        event.preventDefault();
        this.props.loginUser({...this.state});
    };

    handleClose = () => {
        this.setState({open: false});
        this.props.history.push('/')

    };

    render() {
        const {classes} = this.props;
        return (
            <Modal
                aria-labelledby="simple-modal-title"
                aria-describedby="simple-modal-description"
                open={this.state.open}
                onClose={this.handleClose}
            >

                <div className={classes.paper}>
                    <FacebookLogin />

                    <Typography variant="h6" id="modal-title">
                        Login
                    </Typography>
                    <FormGroup className={classes.container}>
                        <TextField
                            label="Name"
                            type="text"
                            name="username"
                            required
                            className={classes.textField}
                            value={this.state.username}
                            onChange={this.inputChangeHandler}
                            margin="normal"
                            error={this.props.error && true}
                            helperText={this.props.error && this.props.error.error}
                            autoComplete="new-username"
                        />

                        <TextField
                            label="Password"
                            className={classes.textField}
                            type="password"
                            name="password"
                            required
                            autoComplete="new-password"
                            error={this.props.error && true}
                            helperText={this.props.error && this.props.error.error}
                            margin="normal"
                            value={this.state.password}
                            onChange={this.inputChangeHandler}
                        />
                        <Button variant="contained" color="secondary" className={classes.button}
                                onClick={this.submitFormHandler}>
                            Sign In
                            <Icon className={classes.leftIcon}>send</Icon>
                        </Button>
                    </FormGroup>
                </div>
            </Modal>

        );
    }
}

const mapStateToProps = state => ({
    error: state.users.loginError
});

const mapDispatchToProps = dispatch => ({
    loginUser: userData => dispatch(loginUser(userData))
});

export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(Login));