import React, {Component} from 'react';
import {getTrackHistory} from "../../store/actions/Actions";
import {connect} from "react-redux";
import ListItem from "@material-ui/core/ListItem/ListItem";
import List from "@material-ui/core/List/List";
import Avatar from "@material-ui/core/Avatar/Avatar";
import ListItemText from "@material-ui/core/ListItemText/ListItemText";
import {withStyles} from '@material-ui/core/styles';
import Divider from "@material-ui/core/Divider/Divider";
import Typography from "@material-ui/core/Typography/Typography";
import VideoModal from "../../components/Modal/VideoModal";

const styles = theme => ({
    root: {
        width: 800,
        maxWidth: 800,
        backgroundColor: theme.palette.background.paper,
    },
    dividerFullWidth: {
        margin: `5px 0 0 ${theme.spacing.unit * 2}px`,
    }
});

class TrackHistory extends Component {

    state = {
        open: false,
        title: null,
        url: null
    };

    componentDidMount() {
        this.props.getTrackHistory()
    }

    openVideoModal = (title, url) => {
        this.setState({
            open: true,
            title: title,
            url: url
        });
    };

    handleClose = () => {
        this.setState({open: false});
    };

    render() {
        const {classes} = this.props;

        let track = this.props.tracks.map(info => (
            <List className={classes.root} key={info._id}>
                <Divider component="li"/>
                <li>
                    <Typography className={classes.dividerFullWidth} color="textSecondary" variant="caption">
                        {info.datetime}
                    </Typography>
                </li>
                <ListItem button onClick={() => this.openVideoModal(info.track.title, info.track.url)}>
                    <Avatar>
                        <i className="material-icons">
                            play_circle_outline
                        </i>
                    </Avatar>
                    <ListItemText primary={info.track.title} secondary={info.track.album.artist.title}/>
                </ListItem>
            </List>
        ));
        return (
            <div style={{width: '800px', margin: '0 auto'}}>
                {track}
                <VideoModal
                    open={this.state.open}
                    handleClose={this.handleClose}
                    title={this.state.title}
                    url={this.state.url}
                />
            </div>
        );
    }
}

const mapStateToProps = state => ({
    tracks: state.trackHistoryReducer.tracksHistory
});

const mapDispatchToProps = dispatch => ({
    getTrackHistory: () => dispatch(getTrackHistory())
});

export default withStyles(styles)(connect(mapStateToProps, mapDispatchToProps)(TrackHistory));