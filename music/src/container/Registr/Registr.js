import React, {Component} from 'react';
import TextField from "@material-ui/core/TextField/TextField";
import {withStyles} from '@material-ui/core/styles';
import Button from "@material-ui/core/Button/Button";
import Icon from '@material-ui/core/Icon';
import FormGroup from "@material-ui/core/FormGroup/FormGroup";
import {registerUser} from "../../store/actions/Actions";
import Modal from '@material-ui/core/Modal';
import {connect} from "react-redux";
import Typography from "@material-ui/core/Typography/Typography";

import FacebookLogin from "../../components/FacebookLogin/FacebookLogin";

const styles = theme => ({
    container: {
        display: 'flex',
        flexWrap: 'wrap',
        width: 250,
        margin: '0 auto'
    },
    textField: {
        marginLeft: theme.spacing.unit,
        marginRight: theme.spacing.unit,
        width: 250,
        alignSelf: 'center'
    },
    button: {
        margin: theme.spacing.unit,
    },
    leftIcon: {
        marginLeft: theme.spacing.unit,
    },
    paper: {
        position: 'absolute',
        top: 100,
        left: '50%',
        marginLeft: -250,
        width: theme.spacing.unit * 50,
        backgroundColor: theme.palette.background.paper,
        boxShadow: theme.shadows[5],
        padding: theme.spacing.unit * 4,
        outline: 'none',
    },
    input: {
        display: 'none',
    },
});

class Registr extends Component {

    state = {
        username: '',
        password: '',
        displayName: '',
        image: '',
        open: true,
        error: null
    };
    inputChangeHandler = event => {
        this.setState({
            [event.target.name]: event.target.value
        });
    };

    fileChangeHandler = event => {
        this.setState({
            [event.target.name]: event.target.files[0]
        });
    };
    submitFormHandler = event => {
        event.preventDefault();
        if (this.state.displayName === '') {
            return this.setState({error: true})
        }

        const formData = new FormData();

        delete this.state.error;
        delete this.state.open;
        Object.keys(this.state).forEach(key => {
              formData.append(key, this.state[key]);
        });

        this.props.registerUser(formData)
    };



    handleClose = () => {
        this.setState({open: false});
        this.props.history.push('/')
    };

    render() {
        const {classes} = this.props;
        return (
            <Modal
                aria-labelledby="simple-modal-title"
                aria-describedby="simple-modal-description"
                open={this.state.open}
                onClose={this.handleClose}
            >

                <div className={classes.paper}>
                    <FacebookLogin/>
                    <Typography variant="h6" id="modal-title">
                        Registration
                    </Typography>
                    <FormGroup className={classes.container}>
                        <TextField
                            label="Name"
                            type="text"
                            name="username"
                            required
                            className={classes.textField}
                            value={this.state.username}
                            onChange={this.inputChangeHandler}
                            margin="normal"
                            error={this.props.error && true}
                            helperText={this.props.error && this.props.error.errors.username && this.props.error.errors.username.message}
                            autoComplete="new-username"
                        />

                        <TextField
                            label="Password"
                            className={classes.textField}
                            type="password"
                            name="password"
                            required
                            autoComplete="new-password"
                            error={this.props.error && true}
                            helperText={this.props.error && this.props.error.errors.password && this.props.error.errors.password.message}
                            margin="normal"
                            value={this.state.password}
                            onChange={this.inputChangeHandler}
                        />

                        <TextField
                            label="Display Name"
                            className={classes.textField}
                            type="text"
                            name="displayName"
                            required
                            margin="normal"
                            error={this.state.error && true}
                            helperText={this.state.error && 'Please add your display name'}
                            value={this.state.displayName}
                            onChange={this.inputChangeHandler}
                        />

                        <input
                            className={classes.input}
                            id="contained-button-file"
                            multiple
                            type="file"
                            name="image"
                            onChange={this.fileChangeHandler}
                        />
                        <label htmlFor="contained-button-file">
                            <Button variant="contained" component="span" className={classes.button}>
                                Download image
                            </Button>
                        </label>
                        <Button variant="contained" color="secondary" className={classes.button}
                                onClick={this.submitFormHandler}>
                            Save
                            <Icon className={classes.leftIcon}>save</Icon>
                        </Button>
                    </FormGroup>
                </div>
            </Modal>

        );
    }
}

const mapStateToProps = state => ({
    error: state.users.registerError
});

const mapDispatchToProps = dispatch => ({
    registerUser: userData => dispatch(registerUser(userData))
});

export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(Registr));