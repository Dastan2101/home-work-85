import React, {Component} from 'react';
import {withStyles} from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import {getArtists} from "../../store/actions/Actions";
import {connect} from "react-redux";
import * as WOW from "wow.js";
import {Link} from "react-router-dom";
import ImageThumbnail from "../../components/ImageThumbNail/ImageThumbNail";
import Grid from "@material-ui/core/Grid/Grid";

const styles = {
    card: {
        maxWidth: 345,
        marginBottom: 20,
    },
    media: {
        objectFit: 'curtain',
    },
    root: {
        flexGrow: 1,
    }

};

class MainPage extends Component {

    componentDidMount() {
        this.props.getArtists();
        new WOW().init();
    }

    render() {
        const {classes} = this.props;

        let artistsList;
        let notPublished;

        if (!this.props.artists) {
            return <div>Loading..</div>
        } else {
            artistsList = this.props.artists.filter(published => published.published === true).map(artist => (
                <div className="wow fadeInDown" data-wow-iteration="1" data-wow-offset="80" key={artist._id}
                     style={{display: 'inline-block', margin: '8px 20px'}}
                >
                    <Card
                        className={classes.card}
                    >

                        <Link to={'/albums/' + artist._id} style={{textDecoration: 'none'}}>
                            <CardActionArea>
                                <ImageThumbnail image={artist.image}/>
                                <CardContent>
                                    <Typography gutterBottom variant="h5" component="h2" color="primary">
                                        {artist.title}
                                    </Typography>
                                    <Typography component="p" color="secondary">
                                        {artist.information}
                                    </Typography>
                                </CardContent>

                            </CardActionArea>
                        </Link>

                    </Card>

                </div>
            ));

            notPublished = this.props.artists.filter(published => published.published === false).map(artist => (
                <div className="wow fadeInDown" data-wow-iteration="1" data-wow-offset="80" key={artist._id}
                >

                    <Card
                        className={classes.card}
                    >
                        <Link to={'/albums/' + artist._id} style={{textDecoration: 'none'}}>

                            <CardActionArea>
                                <ImageThumbnail image={artist.image}/>
                                <CardContent>
                                    <Typography gutterBottom variant="h5" component="h2" color="primary">
                                        {artist.title}
                                    </Typography>
                                    <Typography component="p" color="secondary">
                                        {artist.information}
                                    </Typography>
                                </CardContent>

                            </CardActionArea>
                        </Link>
                    </Card>

                </div>
            ));

        }

        return (
            <div className={classes.root}>
                <Grid container spacing={24}>
                    <Grid item xs={4}>
                        <h3 style={{textAlign: 'center'}}>Not published</h3>
                        {notPublished}
                    </Grid>
                    <Grid item xs={8}>
                        {artistsList}
                    </Grid>
                </Grid>
            </div>
        );
    }
}

const mapStateToProps = state => ({
    artists: state.artists.artists
});

const mapDispatchToProps = dispatch => ({
    getArtists: () => dispatch(getArtists()),
});

export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles,)(MainPage));