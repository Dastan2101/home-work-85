import React, {Component} from 'react';
import {connect} from "react-redux";
import {withStyles} from '@material-ui/core/styles';
import GridList from '@material-ui/core/GridList';
import GridListTile from '@material-ui/core/GridListTile';
import GridListTileBar from '@material-ui/core/GridListTileBar';
import IconButton from '@material-ui/core/IconButton';
import InfoIcon from '@material-ui/icons/Info';
import ListSubheader from "@material-ui/core/ListSubheader/ListSubheader";
import {getAlbums} from "../../store/actions/Actions";
import {Link} from "react-router-dom";
import ImageThumbnail from "../../components/ImageThumbNail/ImageThumbNail";
import Grid from "@material-ui/core/Grid/Grid";


const styles = theme => ({
    root: {
        display: 'flex',
        flexWrap: 'wrap',
        justifyContent: 'space-around',
        overflow: 'hidden',
        backgroundColor: theme.palette.background.paper,
    },
    spin: {
        flexGrow: 1,
    },
    gridList: {
        width: 750,
        height: 500,
    },
    icon: {
        color: 'rgba(255, 255, 255, 0.54)',
    },
    title: {
        color: '#ff0002',
        fontSize: 20
    }
});

class AlbumsOfArtist extends Component {

    componentDidMount() {
        this.props.getAlbums(this.props.match.params.id)
    }

    render() {
        const {classes} = this.props;

        let albums;
        let notPublished;
        if (!this.props.albums) {
            return <div>Loading...</div>
        } else {
            albums = this.props.albums.filter(published => published.published === true).map(album => (
                <GridListTile key={album._id}>
                    <Link to={'/tracks/' + album._id}>
                        <ImageThumbnail image={album.image}/>
                        <GridListTileBar
                            title={album.title}
                            subtitle={<span>Release date: {album.yearOfIssue}</span>}
                            actionIcon={
                                <IconButton className={classes.icon}>
                                    <InfoIcon/>
                                </IconButton>
                            }
                            classes={{
                                title: classes.title
                            }}
                        />
                    </Link>

                </GridListTile>
            ));

            notPublished = this.props.albums.filter(published => published.published === false).map(album => (
                <GridListTile key={album._id}>
                    <Link to={'/tracks/' + album._id}>
                        <ImageThumbnail image={album.image}/>
                        <GridListTileBar
                            title={album.title}
                            subtitle={<span>Release date: {album.yearOfIssue}</span>}
                            actionIcon={
                                <IconButton className={classes.icon}>
                                    <InfoIcon/>
                                </IconButton>
                            }
                            classes={{
                                title: classes.title
                            }}
                        />
                    </Link>

                </GridListTile>
            ));

        }

        return (
            <div className={classes.root}>
                <Grid container spacing={24}>
                    <Grid item xs={4}>
                        <h3 style={{textAlign: 'center'}}>Not published</h3>
                        {notPublished}
                    </Grid>
                    <Grid item xs={8}>
                        <div className={classes.root}>
                            <GridList cellHeight={220} className={classes.gridList}>
                                <GridListTile key="Subheader" cols={2} style={{height: 'auto'}}>
                                    <ListSubheader
                                        component="div">
                                        {this.props.albums && this.props.albums.length > 0
                                            ?
                                            this.props.albums[0].artist && this.props.albums[0].artist.title
                                            :
                                            <h2>Not found</h2>}
                                    </ListSubheader>
                                </GridListTile>
                                {albums}
                            </GridList>
                        </div>
                    </Grid>
                </Grid>
            </div>
        );
    }
}

const mapStateToProps = state => ({
    albums: state.album.albums
});

const mapDispatchToProps = dispatch => ({
    getAlbums: (id) => dispatch(getAlbums(id))
});

export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(AlbumsOfArtist));