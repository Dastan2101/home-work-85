import React, {Component} from 'react';
import {withStyles} from '@material-ui/core/styles';
import InputItem from "../../../components/InputItem/InputItem";
import SaveIcon from '@material-ui/icons/Save';
import Button from '@material-ui/core/Button';
import {createAlbum, getArtists} from "../../../store/actions/Actions";
import {connect} from "react-redux";
import TextField from "@material-ui/core/TextField/TextField";
import MenuItem from "@material-ui/core/MenuItem/MenuItem";

const styles = theme => ({
    container: {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'column'
    },
    button: {
        margin: theme.spacing.unit,
    },
    leftIcon: {
        marginRight: theme.spacing.unit,
    },
    input: {
        display: 'none',
    },
    textField: {
        width: 228,
        margin: 10
    }
});


class AddAlbumForm extends Component {

    state = {
        title: '',
        yearOfIssue: '',
        image: '',
        artist: ''
    };

    componentDidMount() {
        this.props.getArtists()
    }

    inputChangeHandler = event => {
        this.setState({
            [event.target.name]: event.target.value
        })
    };
    fileChangeHandler = event => {
        this.setState({
            [event.target.name]: event.target.files[0]
        });
    };
    submitFormHandler = event => {

        if (this.state.title !== '' && this.state.artist !== '') {
            event.preventDefault();

            const formData = new FormData();

            Object.keys(this.state).forEach(key => {
                formData.append(key, this.state[key]);
            });

            this.props.createAlbum(formData)

        }

    };

    render() {
        const {classes} = this.props;

        return (
            <form className={classes.container} noValidate autoComplete="off" onSubmit={this.submitFormHandler}>
                <InputItem
                    required
                    label="Album name"
                    type="text"
                    value={this.state.title}
                    onChange={this.inputChangeHandler}
                    name="title"
                />
                <InputItem
                    label="Realized"
                    type="number"
                    value={this.state.yearOfIssue}
                    onChange={this.inputChangeHandler}
                    name="yearOfIssue"
                />
                <TextField
                    select
                    label="Artist"
                    className={classes.textField}
                    value={this.state.artist}
                    name="artist"
                    onChange={this.inputChangeHandler}
                    helperText="Please select artist"
                    required
                    variant="outlined"
                >
                    {this.props.artists.map(option => (
                        <MenuItem key={option._id} value={option._id}>
                            {option.title}
                        </MenuItem>
                    ))}
                </TextField>
                <input
                    className={classes.input}
                    id="contained-button-file"
                    multiple
                    type="file"
                    name="image"
                    onChange={this.fileChangeHandler}
                />
                <label htmlFor="contained-button-file">
                    <Button variant="contained" component="span" className={classes.button}>
                        Download image
                    </Button>
                </label>
                <Button variant="contained" color="secondary" size="small" className={classes.button}
                        type="submit">
                    <SaveIcon className={classes.leftIcon}/>
                    Save
                </Button>
            </form>
        );
    }
}

const mapStateToProps = state => ({
    artists: state.artists.artists
});

const mapDispatchToProps = dispatch => ({
    getArtists: () => dispatch(getArtists()),
    createAlbum: (data) => dispatch(createAlbum(data))
});

export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(AddAlbumForm));