import React, {Component} from 'react';
import {withStyles} from '@material-ui/core/styles';
import InputItem from "../../../components/InputItem/InputItem";
import SaveIcon from '@material-ui/icons/Save';
import Button from '@material-ui/core/Button';
import {createTrack, getAlbums, getArtists} from "../../../store/actions/Actions";
import {connect} from "react-redux";
import TextField from "@material-ui/core/TextField/TextField";
import MenuItem from "@material-ui/core/MenuItem/MenuItem";

const styles = theme => ({
    container: {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'column'
    },
    button: {
        margin: theme.spacing.unit,
    },
    leftIcon: {
        marginRight: theme.spacing.unit,
    },
    input: {
        display: 'none',
    },
    textField: {
        width: 228,
        margin: 10
    }
});


class AddTrackForm extends Component {

    state = {
        title: '',
        artist: '',
        album: '',
        duration: '',
        url: ''
    };

    componentDidMount() {
        this.props.getArtists()
    }


    inputChangeHandler = event => {
        this.setState({
            [event.target.name]: event.target.value
        });

    };

    componentDidUpdate(prevProps, prevState, snapshot) {
        if (this.state.artist !== prevState.artist) {
            this.props.getAlbums(this.state.artist)
        }
    }

    submitFormHandler = event => {

        if (this.state.title !== '' && this.state.album !== '') {
            event.preventDefault();

            this.props.createTrack({...this.state})
        }

    };

    render() {
        const {classes} = this.props;

        return (
            <form className={classes.container} noValidate autoComplete="off" onSubmit={this.submitFormHandler}>
                <InputItem
                    required
                    label="Track name"
                    type="text"
                    value={this.state.title}
                    onChange={this.inputChangeHandler}
                    name="title"
                />
                <InputItem
                    label="Duration example x:xx"
                    type="text"
                    value={this.state.duration}
                    onChange={this.inputChangeHandler}
                    name="duration"
                />
                <TextField
                    select
                    label="Artist"
                    className={classes.textField}
                    value={this.state.artist}
                    helperText="Please select artist"
                    onChange={this.inputChangeHandler}
                    required
                    variant="outlined"
                    name="artist"
                >
                    {this.props.artists.map(option => (
                        <MenuItem key={option._id} value={option._id}>
                            {option.title}
                        </MenuItem>
                    ))}
                </TextField>
                <TextField
                    select
                    label="Album"
                    className={classes.textField}
                    value={this.state.album}
                    name="album"
                    onChange={this.inputChangeHandler}
                    helperText="Please select album"
                    required
                    variant="outlined"
                >
                    {this.props.album.map(option => (
                        <MenuItem key={option._id} value={option._id}>
                            {option.title}
                        </MenuItem>
                    ))}
                </TextField>
                <input
                    style={{width: '228px', margin: '10px', height: '40px', borderRadius: '4px'}}
                    placeholder=" Url "
                    type="url"
                    value={this.state.url}
                    onChange={this.inputChangeHandler}
                    name="url"
                />
                <Button variant="contained" color="secondary" size="small" className={classes.button}
                        type="submit">
                    <SaveIcon className={classes.leftIcon}/>
                    Save
                </Button>
            </form>
        );
    }
}

const mapStateToProps = state => ({
    artists: state.artists.artists,
    album: state.album.albums
});

const mapDispatchToProps = dispatch => ({
    getArtists: () => dispatch(getArtists()),
    getAlbums: (id) => dispatch(getAlbums(id)),
    createTrack: (data) => dispatch(createTrack(data))
});

export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(AddTrackForm));