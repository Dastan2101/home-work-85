import React, {Component} from 'react';
import {withStyles} from '@material-ui/core/styles';
import InputItem from "../../../components/InputItem/InputItem";
import SaveIcon from '@material-ui/icons/Save';
import Button from '@material-ui/core/Button';
import {createArtist} from "../../../store/actions/Actions";
import {connect} from "react-redux";

const styles = theme => ({
    container: {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'column'
    },
    button: {
        margin: theme.spacing.unit,
    },
    leftIcon: {
        marginRight: theme.spacing.unit,
    },
    input: {
        display: 'none',
    }
});


class AddArtistForm extends Component {

    state = {
        title: '',
        information: '',
        image: ''
    };

    inputChangeHandler = event => {
        this.setState({
            [event.target.name]: event.target.value
        })
    };
    fileChangeHandler = event => {
        this.setState({
            [event.target.name]: event.target.files[0]
        });
    };
    submitFormHandler = event => {

        if (this.state.title !== '') {
            event.preventDefault();

            const formData = new FormData();

            Object.keys(this.state).forEach(key => {
                formData.append(key, this.state[key]);
            });

            this.props.createArtist(formData)

        }

    };

    render() {
        const {classes} = this.props;

        return (
            <form className={classes.container} noValidate autoComplete="off" onSubmit={this.submitFormHandler}>
                <InputItem
                    required
                    label="Artist name"
                    type="text"
                    value={this.state.title}
                    onChange={this.inputChangeHandler}
                    name="title"
                />
                <InputItem
                    label="Information"
                    type="text"
                    value={this.state.information}
                    onChange={this.inputChangeHandler}
                    name="information"
                />
                <input
                    className={classes.input}
                    id="contained-button-file"
                    multiple
                    type="file"
                    name="image"
                    onChange={this.fileChangeHandler}
                />
                <label htmlFor="contained-button-file">
                    <Button variant="contained" component="span" className={classes.button}>
                        Download image
                    </Button>
                </label>
                <Button variant="contained" color="secondary" size="small" className={classes.button}
                        type="submit">
                    <SaveIcon className={classes.leftIcon}/>
                    Save
                </Button>
            </form>
        );
    }
}

const mapDispatchToProps = dispatch => ({
    createArtist: data => dispatch(createArtist(data))
});

export default connect(null, mapDispatchToProps)(withStyles(styles)(AddArtistForm));