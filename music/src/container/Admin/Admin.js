import React, {Component} from 'react';
import {createPublish, deletePublic, getAllEntities} from "../../store/actions/Actions";
import {connect} from "react-redux";
import {withStyles} from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Typography from '@material-ui/core/Typography';
import TableContainer from "../../components/TableContainer/TableContainer";

import './Admin.css';

const styles = theme => ({
    root: {
        flexGrow: 1,
        width: 800,
        margin: '0 auto',
    },
    tableRoot: {
        width: '100%',
        marginTop: theme.spacing.unit * 3,
        overflowX: 'auto',
    },
    table: {
        minWidth: 700,
    },
    margin: {
        margin: theme.spacing.unit,
    },
    padding: {
        padding: 24
    }
});

class Admin extends Component {
    state = {
        value: 0,
    };

    componentDidMount() {
        this.props.getAllEntities();
    }


    handleChange = (event, value) => {
        this.setState({value});
    };


    render() {
        const {classes} = this.props;
        const {value} = this.state;

        return (
            <div className={classes.root}>
                <AppBar position="static">
                    <Tabs value={value} onChange={this.handleChange} className={classes.tabs}>
                        <Tab label="Artists"/>
                        <Tab label="Albums"/>
                        <Tab label="Tracks"/>
                    </Tabs>
                </AppBar>
                {value === 0 && <Typography component="div" className={classes.padding}>
                    <TableContainer
                        array={this.props.artists}
                        onClick={this.props.createPublish}
                        onDelete={this.props.deletePublic}/>
                </Typography>}
                {value === 1 && <Typography component="div" className={classes.padding}>
                    <TableContainer
                        array={this.props.albums}
                        onClick={this.props.createPublish}
                        onDelete={this.props.deletePublic}/>
                </Typography>}
                {value === 2 && <Typography component="div" className={classes.padding}>
                    <TableContainer
                        array={this.props.tracks}
                        onClick={this.props.createPublish}
                        onDelete={this.props.deletePublic}/>
                </Typography>}
            </div>
        );
    }
}

const mapStateToProps = state => ({
    artists: state.admin.artists,
    albums: state.admin.albums,
    tracks: state.admin.tracks
});

const mapDispatchToProps = dispatch => ({
    getAllEntities: () => dispatch(getAllEntities()),
    createPublish: (id) => dispatch(createPublish(id)),
    deletePublic: id => dispatch(deletePublic(id))
});

export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(Admin));

