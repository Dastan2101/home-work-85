import React, {Component} from 'react';
import {withStyles} from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import {connect} from "react-redux";
import IconButton from "@material-ui/core/IconButton/IconButton";
import * as WOW from "wow.js";
import {getTracks, postHistoryTrack} from "../../store/actions/Actions";
import VideoModal from "../../components/Modal/VideoModal";
import Button from "@material-ui/core/Button/Button";
import Icon from "@material-ui/core/Icon/Icon";
import Grid from "@material-ui/core/Grid/Grid";

const styles = theme => ({
    root: {
        width: '100%',
        maxWidth: 360,
        backgroundColor: theme.palette.background.paper,
    },
    button: {
        margin: theme.spacing.unit,
    },
    leftIcon: {
        marginLeft: theme.spacing.unit,
    }
});


class TracksList extends Component {

    state = {
        open: false,
        title: null,
        url: null
    };

    componentDidMount() {
        new WOW().init();
        this.props.getTracks(this.props.match.params.id)
    }

    openVideoModal = (title, url) => {
        this.setState({
            open: true,
            title: title,
            url: url
        });
    };
    handleClose = () => {
        this.setState({open: false});
    };

    render() {
        let tracks;
        let notPublished;
        if (!this.props.tracks) {
            tracks = <div>Loading...</div>

        } else {
            tracks = this.props.tracks.filter(published => published.published === true).map(track => (
                <div className="wow bounceInLeft" data-wow-iteration="1" data-wow-offset="80" key={track._id}
                     data-wow-delay={'0.' + this.props.tracks.indexOf(track) + 's'}
                     onClick={() => this.props.postHistoryTrack(track._id)}
                >
                    <List component="nav" className={styles.root}>
                        <ListItem button>
                            <span>{track.number}</span>
                            <ListItemText inset primary={track.title}/>
                            <IconButton aria-label="Comments">
                                {track.duration}
                            </IconButton>
                        </ListItem>
                        <Button variant="contained" color="secondary" className={styles.button}
                                onClick={() => this.openVideoModal(track.title, track.url)}>
                            Смотреть
                            <Icon className={styles.leftIcon}>
                                <i className="material-icons">
                                    play_circle_filled
                                </i>
                            </Icon>
                        </Button>
                    </List>
                </div>

            ));

            notPublished = this.props.tracks.filter(published => published.published === false).map(track => (
                <div className="wow bounceInLeft" data-wow-iteration="1" data-wow-offset="80" key={track._id}
                     data-wow-delay={'0.5s'}
                >
                    <List component="nav" className={styles.root}>
                        <ListItem button>
                            <span>{track.number}</span>
                            <ListItemText inset primary={track.title}/>
                            <IconButton aria-label="Comments">
                                {track.duration}
                            </IconButton>
                        </ListItem>
                    </List>
                </div>
            ));
        }



        return (

            <div className={styles.root}>
                <Grid container spacing={24}>
                    <Grid item xs={4}>
                        <h3 style={{textAlign: 'center'}}>Not published</h3>
                        {notPublished}
                    </Grid>
                    <Grid item xs={8}>
                        <div style={{width: '400px', margin: '20px auto'}}>
                            <h2>{this.props.tracks && this.props.tracks.length > 0
                                ?
                                this.props.tracks[0].artist && this.props.tracks[0].artist.title
                                :
                                <span>Not found</span>}</h2>
                            {tracks}
                            <VideoModal
                                open={this.state.open}
                                handleClose={this.handleClose}
                                title={this.state.title}
                                url={this.state.url}
                            />
                        </div>
                    </Grid>
                </Grid>
            </div>
        );
    }
}

const mapStateToProps = state => ({
    tracks: state.track.tracks
});

const mapDispatchToProps = dispatch => ({
    getTracks: (id) => dispatch(getTracks(id)),
    postHistoryTrack: trackId => dispatch(postHistoryTrack(trackId))
});

export default connect(mapStateToProps, mapDispatchToProps)((withStyles(styles))(TracksList));